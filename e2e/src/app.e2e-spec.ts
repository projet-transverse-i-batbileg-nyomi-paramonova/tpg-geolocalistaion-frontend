import { AppPage } from './app.po';
import { browser, logging, element } from 'protractor';
const { percySnapshot } = require('@percy/protractor')


// import * as angular from 'angular';
import { of } from 'rxjs';

describe('regression test', () => {
  let page: AppPage;

  beforeEach(() => {
    //   browser.addMockModule('PositionService', () => {
    //     angular.module('PositionService', [])
    //     .value({
    //       sendPosition(position) {},
    //       getPositions() {
    //         return of([{lat: 0, lng: 0}]);
    //       }
    //     });
    //   });
    page = new AppPage();
  });


  it('should load home page', async () => {
    await browser.get(browser.baseUrl + '/line-selection');
    page.navigateTo();
    await percySnapshot('Home Page');
  });

  it('should load bus selection page', async () => {
    await browser.get(browser.baseUrl + '/line-choice');
    await percySnapshot('Bus selection');
  });

  it('should load the map', async () => {
    await browser.get(browser.baseUrl + '/live-map');
    await percySnapshot('Map');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    // expect(logs).not.toContain(jasmine.objectContaining({
    //   level: logging.Level.SEVERE,
    // } as logging.Entry));
  });
});
