import { Component, OnInit, ViewChild } from '@angular/core';
import { PositionService } from '../position.service';
import * as mapJson from '../shared/maps.json';
import { GoogleMap } from '@angular/google-maps';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';


interface RoadOfInterest {
  type: string;
  coordinates: [];
}

interface Point {
  lat: number;
  lng: number;
}

interface Segment {
  start: Point;
  end: Point;
}

interface Arret {
  arrival: number;
  lat: number;
  lng: number;
}

@Component({
  selector: 'app-live-map',
  templateUrl: './live-map.component.html',
  styleUrls: ['./live-map.component.css']
})
export class LiveMapComponent implements OnInit {
  lat = 46.204375;
  lng = 6.150004;
  markers: Point[] = [];
  stops = new Set();
  stopsDisplay = new Set();
  linesOfInterest = [];
  lines = [];
  segments: Segment[] = [];
  segPoints: Point[] = [];
  roadOfInterest: RoadOfInterest;
  selectedLine = '';
  selectedStop = '';
  destination = '';
  destinations = new Set();
  mapStyle = (mapJson as any).default;
  getCoordOnly = '';
  coordinates = [];
  index = 0;
  arrets: Arret[] = [];

  offset = 0;

  @ViewChild(GoogleMap, { static: false })
  mapContainer: GoogleMap; // stocking our map in var map


  bus = {
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
    scale: 9,
    strokeColor: '#393'
  };

  line: google.maps.Polyline;

  constructor(private positionService: PositionService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log('Initialized', this.markers);


    this.positionService.getPositions().subscribe((positionUpdate: Point) => {
      this.markers.push(positionUpdate);
    });

    // this.positionService.getAllLigns().subscribe(lines => {
    //   // console.log("invisible log:", lines);
    //   this.lines = lines;
    // });

    this.route.params.subscribe(params => {
      this.getThermometer(params.line, params.arret, params.dest);
      if (params && !navigator.geolocation) {
        console.log('Your browser does not support geolocation');
      } else {
        console.log('Geolocation supported user will be prompted to share his location');
        console.log('sending', this.positionService.sending);
        if (this.positionService.sending) {
          navigator.geolocation.watchPosition(geoUpdate => {
            console.log('new position', geoUpdate);
            const userPos = {
              lat: geoUpdate.coords.latitude,
              lng: geoUpdate.coords.longitude,
              timestamp: geoUpdate.timestamp,
            };
            this.positionService.sendPosition(userPos);
          });
        }
      }
    });



  }

  getDemo() {
    this.positionService.getDemo();
  }

  getStops() {
    this.positionService.getStops();
  }

  getLines(stop) {
    console.log('getting lines for : ', stop);
    this.positionService.getLignsForStop(stop);
    this.positionService.subscribeStops(stop).subscribe(lines => {
      console.log('Got lines: ', lines);
      this.linesOfInterest = lines;
      console.log(lines, '<----LINES');
      this.lines = this.lines;
      // this.lines = this.lines.sort((one, two) => (one > two ? -1 : 1));
    });



  }

  // sortLen(one, two){(one.length > two.length ? -1 : 1);}


  getNextPositions(stopCode, line, destination) {
    this.positionService.getNextPositions(stopCode, line, destination);
  }

  filterDestinations(filter, line) {
    this.positionService.getDestination(filter, line);
  }

  // Final implementation
  getSetupData() {
    this.positionService.startSubscription().subscribe(data => {
      // console.log(data);
      data.forEach(entry => {
        // console.log(entry);
        this.destinations.add(entry.destination);
        // this.lines.push(entry.ligne);
        this.stops.add(entry.nom_arret);
      });
    });
  }

  getThermometer(line, stop, dest) {
    this.positionService.connectGivenChannel(line, stop, dest).subscribe(data => {
      console.log(data);
      data = data.filter(datum => datum);
      this.arrets = [];
      for (const [i, a] of data.entries()) {
        this.arrets.push({
          arrival: a.arrivalTime,
          lat: a.coordinates.latitude,
          lng: a.coordinates.longitude,
        });
      }
      let finaltime = 0;
      const finalStop = this.arrets[this.arrets.length - 1];
      finaltime = finalStop.arrival;
      const mapLine = new google.maps.Polyline({
        path: this.arrets,
        icons: [{
          icon: this.bus,
          repeat: '0%',
        }],
        map: this.mapContainer._googleMap // on accede à l'objet map de GoogleMap mais de type de Map ( ce qui est le _googleMap)
      });
      this.animatedBus(mapLine, finaltime);
      console.log(this.arrets);
    }
    );
  }

  animatedBus(line, finaltime) {
    let count = 0;
    let dist = 0;
    dist = 1 / (finaltime * 60);
    window.setInterval(() => {
      count = (count + dist);
      const icons = line.icons;
      icons[0].offset = (count) + '%';
      line.set('icons', icons);
    }, 10);
  }
}

