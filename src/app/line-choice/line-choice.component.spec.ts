import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineChoiceComponent } from './line-choice.component';

describe('LineChoiceComponent', () => {
  let component: LineChoiceComponent;
  let fixture: ComponentFixture<LineChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineChoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
