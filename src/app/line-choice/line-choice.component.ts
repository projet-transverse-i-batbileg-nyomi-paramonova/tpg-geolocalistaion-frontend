import { Component, OnInit } from '@angular/core';
import { from, Observable } from 'rxjs';
import { PositionService } from '../position.service';
import { filter } from 'minimatch';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-line-choice',
  templateUrl: './line-choice.component.html',
  styleUrls: ['./line-choice.component.css']
})
export class LineChoiceComponent implements OnInit {
  lines$: Observable<any[]>; // $ is convention in angular to mark an observable. 
  // It means the value might not be accessible yet depending on the service, API and/or network
  destinations: any[];
  $arrets: Observable<any[]>;
  arretChoix: String;
  lineChoix: String;


  constructor(private positionService: PositionService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.lines$ = this.positionService.getAllLigns();
    // this.router.events.subscribe(event => {
    //   if (event instanceof NavigationEnd) {

    //     this.$lines = this.positionService.getAllLigns();
    //   }
    // })
    this.route.queryParams.subscribe(params => {
      this.positionService.sending = params.sendFlag;
    });
  }
  //this.$arrets = this.positionService.getStopsForLine();

  getStops(line) {
    this.positionService.getStopsForLine(line);
    this.$arrets = this.positionService.subLine(line);
    this.lineChoix = line;
  }

  getTheDirection(nom_arret, listeArrets) {
    this.destinations = listeArrets.filter(stop => stop.name === nom_arret);
    this.arretChoix = nom_arret;

  }

  sendInfo(arret, line, dest) {
    this.router.navigate(["live-map", { arret, line, dest }]);
  }
}
