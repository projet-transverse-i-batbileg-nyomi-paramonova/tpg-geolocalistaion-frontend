import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LineChoiceComponent } from './line-choice/line-choice.component';
import { LiveMapComponent } from './live-map/live-map.component';
import { SendGetComponent } from './send-get/send-get.component';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';




const routes: Routes = [
  { path: 'send-get', component: SendGetComponent },
  { path: 'live-map', component: LiveMapComponent },
  { path: 'line-choice', component: LineChoiceComponent },
  { path: '', redirectTo: '/send-get', pathMatch: 'full' }, // redirect to  send get choice page
  { path: '**', component: SendGetComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
