import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PositionService {
  private url = 'http://localhost:3000';
  private socket;
  private channelSocket;
  sending = false;

  constructor() {
    this.socket = io(this.url);
  }

  public sendPosition(position) {
    // Emitting a volatile position as it shouldn't lag if the user is not able to send his position...
    // this.channelSocket.emit('new-position', position);
    console.log(this.channelSocket.toString().split('#')[0]);
    this.socket.emit('new-position', position);
    console.log(position);
  }

  public getPositions() {
    return new Observable(observer => {
      this.socket.on('position', data => {
        observer.next(data);
      });
    });
  }

  public getDemo() {
    this.socket.emit('get-demo');
  }

  public getStops() {
    this.socket.emit('get-stops');
  }

  // FIXME:   This is actually the logic for connections will have to be fixed in backend.
  public getLignsForStop(stop) {
    this.socket.emit('get-lines', stop);
  }

  public getStopsForLine(line) {
    this.socket.emit('get-stops', line);
  }

  public getPreciseRoad(line) {
    this.socket.emit('get-road', line);
  }

  public subPreciseRoad(line) {
    return new Observable<any>(observer => {
      this.socket.on('road', coords => {
        console.log(JSON.parse(coords.coords));
        observer.next(coords.coords);
      });
    });
  }

  public subscribeStops(stop) {
    // Moved out of getLinesForStops because apparetnly doesn't work like that
    return new Observable<Array<any>>(observer => {
      this.socket.on(stop, data => {
        // console.log(data);
        observer.next(data);
      });
    });
  }

  public subLine(line) {
    return new Observable<Array<any>>(observer => {
      this.socket.on(line, data => {
        console.log('responding to line subscription', line);
        // console.table(data);
        observer.next(data);
      });
    });
  }

  public stopsObservable() {
    return new Observable<Array<any>>(observer => {
      this.socket.on('stops', data => {
        // console.table(data);
        observer.next(data);
      });
    });
  }

  public getAllLigns() {
    return new Observable<Array<any>>(observer => {
      this.socket.on('lines', data => {
        // console.log('Getting lines', data);

        observer.next(data.sort(function (a, b) {
          const numA = parseInt(a.numero_ligne);
          const numB = parseInt(b.numero_ligne);
          if (numA && numB) {

            return numA - numB;
          } else {
            return a.numero_ligne.localeCompare(b.numero_ligne);
          }

        }));
      });
    });
  }

  public getNextPositions(physicalStopCode, line, destinationCode) {
    this.socket.emit('get-next-positions', physicalStopCode, line, destinationCode);
    // return new Observable<Array<any>>(observer => {
    //   this.socket.on('')
    // })
  }

  public getDestination(physicalStop, line) {
    this.socket.emit('get-destination', physicalStop, line);
  }

  public subDestination() {
    return new Observable<Array<any>>(observer => {
      this.socket.on('destination', data => {
        console.log('Received destinations', data);
        observer.next(data);
      });
    });
  }

  // Anything that comes after this is for the final version
  public startSubscription() {
    return new Observable<Array<any>>(observer => {
      this.socket.on('start', startData => observer.next(startData));
    });
  }

  public connectGivenChannel(line, stop, dest) {
    console.log('connecting to channel', `${this.url}/${line}/${stop.replace(/\s/g, '')}/${dest.replace(/\s/g, '')}`);
    this.channelSocket = io.connect(`${this.url}/${line}/${stop.replace(/\s/g, '')}/${dest.replace(/\s/g, '')}`);
    return new Observable<Array<any>>(observer => {
      this.channelSocket.on('next-stops', data => {
        console.log('Response from stops');
        console.log(data);
        observer.next(data);
      });
    });
  }
}
