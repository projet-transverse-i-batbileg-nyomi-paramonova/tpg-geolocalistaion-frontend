import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendGetComponent } from './send-get.component';

describe('SendGetComponent', () => {
  let component: SendGetComponent;
  let fixture: ComponentFixture<SendGetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendGetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
